#!/bin/bash

start() {
    chmod -R 0777 /var/www/web/assets
	chmod -R 0777 /var/www/runtime/
	service php7.0-fpm start
    start-stop-daemon --start --pidfile /var/run/sshd.pid --exec /usr/sbin/sshd -- -p 22
    service postgresql start
	service nginx start
	echo "READY"
    fsmonitor -q -d /var/www/docker "+//default.conf" bash -c "nginx -s reload &&  echo 'nginx reload with default.conf'"
}

case "${1}" in
	bash) 
		bash
		;;
	*)             
		start
		;;
esac